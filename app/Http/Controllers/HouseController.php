<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHouseRequest;
use App\Models\Currency;
use App\Models\House;
use App\Traits\Upload;
use App\Models\Photo;
use Illuminate\Http\RedirectResponse;
use Intervention\Image\Facades\Image;

class HouseController extends Controller
{
    use Upload;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show house create form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function create()
    {
        $this->authorize('edit');

        $currencies = Currency::all();

        return view('houses.create',['currencies' => $currencies]);
    }

    /**
     * Creates house.
     *
     * @param StoreHouseRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(StoreHouseRequest $request)
    {
        $this->authorize('edit');

        $house = new House();
        $house->name = $request->name;
        $house->address= $request->address;
        $house->currency_id = $request->currency;

        if ($request->hasFile('photo'))
        {
            $path = $this->UploadFile($request->file('photo'), 'houses');
            $img = Image::make(storage_path('app/public/'.$path))->resize(200, 200);
            $img->save(storage_path('app/public/'.$path));
            $photo = Photo::create([
                'path' => $path
            ]);;

            $house->photo_id = $photo->id;
        }

        if (1 == $house->currency_id)
        {
            $house->price_usd = $request->price;
            $house->price_rub = 0;
        }
        else
        {
            $house->price_usd = 0;
            $house->price_rub = $request->price;
        }

        try
        {
            $house->save();
        }
        catch (\Exception $e)
        {
            throw new \Exception('Error saving house! '.$e->getMessage());
        }

        return redirect()->route('home');
    }

    /**
     * Updates house.
     *
     * @param integer $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(int $id)
    {
        $this->authorize('edit');

        $currencies = Currency::all();
        $house = House::find($id);

        return view('houses.edit',['currencies' => $currencies, 'house' => $house]);
    }
    /**
     * Updates house.
     *
     * @param integer $id
     * @param StoreHouseRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function edit(int $id, StoreHouseRequest $request)
    {
        $this->authorize('edit');

        $house = House::find($id);

        if (!$house) throw new \Exception('House not found!');

        $house->name = $request->name;
        $house->address = $request->address;
        $house->currency_id = $request->currency;

        if ($request->hasFile('photo'))
        {
            $path = $this->UploadFile($request->file('photo'), 'houses');
            $img = Image::make(storage_path('app/public/'.$path))->resize(200, 200);
            $img->save(storage_path('app/public/'.$path));
            $photo = Photo::find($house->photo_id);
            if ($photo)
            {
                unlink(storage_path('app/public/'.$photo->path));
                $photo->path = $path;
                $photo->save();
            }
            else
            {
                $photo = Photo::create([
                    'path' => $path
                ]);
                $house->photo_id = $photo->id;
            }
        }

        if (1 == $house->currency_id)
        {
            $house->price_usd = $request->price;
            $house->price_rub = 0;
        }
        else
        {
            $house->price_usd = 0;
            $house->price_rub = $request->price;
        }

        try
        {
            $house->save();
        }
        catch (\Exception $e)
        {
            throw new \Exception('Error saving house! '.$e->getMessage());
        }

        return redirect()->route('home');
    }
}
