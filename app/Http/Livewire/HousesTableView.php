<?php

namespace App\Http\Livewire;

use App\Actions\EditHouseAction;
use App\Actions\DeleteHouseAction;
use LaravelViews\Views\TableView;
use App\Models\House;
use Illuminate\Support\Facades\Auth;

class HousesTableView extends TableView
{
    /**
     * Sets a model class to get the initial data
     */
    protected $model = House::class;

    /**
     * Sets the headers of the table as you want to be displayed
     *
     * @return array<string> Array of headers
     */
    public function headers(): array
    {
        return ['Name', 'Address', 'Photo', 'Price(RUB)', 'Price(USD)', 'Created', 'Updated'];
    }

    /**
     * Sets the data to every cell of a single row
     *
     * @param $model House model for each row
     */
    public function row($model): array
    {
        return [
            $model->name,
            $model->address,
            ($model->photo_id ? '<img src="'.asset('storage/'.$model->photo["path"]).'" >' : ''),
            $model->price_rub,
            $model->price_usd,
            $model->created_at,
            $model->updated_at
        ];
    }

    protected function actionsByRow()
    {
        if (Auth::user()->name !== 'manager')
        {
            return [
                new EditHouseAction,
                new DeleteHouseAction,
            ];
        }
        return [];
    }
}
