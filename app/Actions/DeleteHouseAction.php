<?php

namespace App\Actions;

use LaravelViews\Actions\Action;
use LaravelViews\Views\View;
use App\Models\Photo;

class DeleteHouseAction extends Action
{
    /**
     * Any title you want to be displayed
     * @var String
     * */
    public $title = "Delete house";

    /**
     * This should be a valid Feather icon string
     * @var String
     */
    public $icon = "delete";

    /**
     * Execute the action when the user clicked on the button
     *
     * @param $model Model object of the list where the user has clicked
     * @param $view Current view where the action was executed from
     */
    public function handle($model, View $view)
    {
        $photo = Photo::find($model->photo_id);
        if ($photo) {
            unlink(storage_path('app/public/'.$photo->path));
            $photo->delete();
        }
        $model->delete();
    }
}
