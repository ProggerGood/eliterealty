<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class House extends Model
{
    use HasFactory;

    protected $fillable = ['name, address, photo_id, currency, price_rub, price_usd'];

    /**
     * Get the photo associated with the house.
     */
    public function photo(): HasOne
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    /**
     * Get the currency associated with the house.
     */
    public function currency(): HasOne
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }
}
