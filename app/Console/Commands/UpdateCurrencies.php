<?php

namespace App\Console\Commands;

use App\Models\House;
use Illuminate\Console\Command;
use AmrShawky\LaravelCurrency\Facade\Currency;

class UpdateCurrencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update currencies and prices';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $houses = House::all();
        foreach($houses as $house)
        {
            if ($house->currency->name == 'RUB') {
                $house->price_usd = Currency::convert()
                    ->from('RUB')
                    ->to('USD')
                    ->amount($house->price_rub)
                    ->get();
            } else {
                $house->price_rub = Currency::convert()
                    ->from('USD')
                    ->to('RUB')
                    ->amount($house->price_usd)
                    ->get();
            }

            $house->save();
        }
    }
}
