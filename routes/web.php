<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('auth')->name('home');
Route::get('/house/create', [App\Http\Controllers\HouseController::class, 'create'])->middleware('auth')->name('create');
Route::post('/house/store', [App\Http\Controllers\HouseController::class, 'store'])->middleware(['auth','optimizeImages'])->name('store');
Route::get('/house/show/{id}', [App\Http\Controllers\HouseController::class, 'show'])->middleware('auth')->name('show');
Route::post('/house/edit/{id}', [App\Http\Controllers\HouseController::class, 'edit'])->middleware('auth')->name('edit');

