<h1>ELite Realty application</h1>
<h2>Initialization</h2>
<ul>
<li>Install migrations: <b>php artisan migrate:install</b></li>
<li>Migrate tables: <b>php artisan migrate</b></li>
<li>Fill data: <b>php artisan db:seed</b></li>
<li>Build assets: <b>npm run build</b></li>
</ul>
<p>
Now start server: <b>php artisan serve</b><br>
Open your browser: <b>http://127.0.0.1:8000</b>
</p>
Test users:
<table>
<tr>
<td>Login</td><td>Password</td>
</tr>
<tr>
<td>admin@example.com</td><td>test</td>
</tr>
<tr>
<td>manager@example.com</td><td>test</td>
</tr>
</table>
<p>
For update houses prices run in console:<br> 
<b>php artisan app:update-currencies</b>
</p>
