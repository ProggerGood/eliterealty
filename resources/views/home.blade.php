@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (Auth::user()->name !== 'manager')
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <a class="btn btn-link" href="{{ route('create') }}">  <button class="btn btn-secondary">{{ __('Create house') }}</button></a>
                </div>
            </div>
            @endif
            @livewire('houses-table-view')
        </div>
    </div>
</div>
@endsection
